<?php

namespace Drupal\vidyo_conference\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
/**
 * Provides a 'VidyoConferenceBlock' block.
 *
 * @Block(
 *  id = "vidyo_conference_block",
 *  admin_label = @Translation("Vidyo conference block"),
 * )
 */
class VidyoConferenceBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
	$build = [];

	if(\Drupal::routeMatch()->getParameter('node')) {
	    $node_id = \Drupal::routeMatch()->getParameter('node')->Id();
	    //$build['vidyo_conference_block']['#markup'] = 'Implement VidyoConferenceBlock.';
	    $db = \Drupal::database();
	    $query = $db->select('host_create_appointment', 'cie');
	    $query->fields('cie', ['meeting_nid', 'host_token', 'participant_token', 'participants_details']);
	    $query->condition('cie.meeting_nid', $node_id, '=');
		$data = $query->execute()->fetchAll();   
		
		$current_time = date("y-m-d H:i:s",\Drupal::time()->getCurrentTime());

	    if(!empty($data)) {	
			//getting start and end time of meeting
			$node = \Drupal::entityTypeManager()->getStorage('node')->load($node_id);   
			$start_time = $node->get('field_meeting_start_time')->value;
			$end_time = $node->get('field_meeting_end_time')->value;

			$dt=new \DateTime($start_time, new \DateTimeZone('UTC'));
			$dt->setTimeZone(new \DateTimeZone('Asia/Kolkata'));
			$start_time =  $dt->format('Y-m-d H:i:s');

			$dt=new \DateTime($end_time, new \DateTimeZone('UTC'));
			$dt->setTimeZone(new \DateTimeZone('Asia/Kolkata'));
			$end_time =  $dt->format('Y-m-d H:i:s');
			//$start_time = str_replace("T", " ", $start_time);
			
			if($current_time <= $end_time){
			$participants_details = unserialize($data['participants_details']);
			// ------ Generate token -----------
    		// 1. Get Application ID		     
			$videoConfig = \Drupal::config('vidyo_conference.vidyoconfig');
			$app_id = $videoConfig->get('application_id');
			// 2. Get Developer Key
			$developer_key = $videoConfig->get('developer_key');
			// 3. Get Default expiration time
			$end_calc_time = strtotime(date_format(date_create(_format_date_time($node->get('field_meeting_end_time')->getValue()[0]['value'])), 'Y-m-d H:i:s'));
    		$differenceInSeconds = ( (($end_calc_time - time()) * 60 * 60) / 3600);
			$expirationTime = $videoConfig->get('default_expiration_time');
			$EPOCH_SECONDS = 62167219200 ;
			if ($differenceInSeconds) {
				$expirationTime = $differenceInSeconds;
			}
			else {
				$expirationTime = $videoConfig->get('default_expiration_time'); // Generated token will expire after these many seconds
			  }  
			$expires = $EPOCH_SECONDS + $expirationTime + time();
			//get current username
			$currernt_username = \Drupal::currentUser()->getUsername();
			$author_name= $node->getOwner()->getDisplayName();
			$account = \Drupal\user\Entity\User::load($node->toArray()['field_participant'][0]['target_id']); // pass your uid
			$participant_name= $account->getUsername();
			    if ($currernt_username == $author_name) {
				  $token = _generate_tokens($author_name, $app_id,  $expires, $developer_key);
			      //$token = $data['host_token'];
				}
				else {
					$token = _generate_tokens($participant_name, $app_id,  $expires, $developer_key);
			 	  //$token = $data['participant_token'];
				}
				// $build['vidyo_conference_block']['#markup'] = 'token'.$token;

			    $build =  array(
			      '#meeting_nid'   => $node_id,
			      '#token' 	 		   => $token,
			      '#current_user_name' => $current_user_name,       
			      '#theme' 			   => 'vidyoconference',
			      '#attached' 		   => array(
			        'library' 		 => array('vidyo_conference/vidyoconferenceblock'),
			      ),
				);
				return $build;
				
			}
			else {
				$build['vidyo_conference_block']['#markup'] = 'wait for start time'.$start_time.' & '.$current_time.' end time'.$end_time;
				return $build;
			}
	    }
	    else {
			$build['vidyo_conference_block']['#markup'] = 'No Virtual Appointment is set.';
			return $build;
		}
	}
	
	 
  }
  function _generate_tokens($currernt_user_name, $app_id,  $expire_time, $developer_key) {
	// Now generate token
	$jid = $currernt_user_name . "@" . $app_id ;
	 
	// Must place \0 within double quotes, not single quotes.
	$body = "provision" . "\0" . $jid . "\0" . $expire_time . "\0" . "";
 
	// Important to convert to UTF8.
	$utf8_body = utf8_encode($body) ;
 
	// Ensure the SHA384 Algo is being used.
	$mac_hash = hash_hmac("sha384", $utf8_body, $developer_key);
 
	// Again, ensure \0 are encapsulated with double quotes. 
	// Single quotes does not append the null character to the string
	$serialized = $utf8_body . "\0" . $mac_hash;
 
	// Base64 Encode the serialized string
	$token = base64_encode($serialized);


	 //$command = "python generateToken.py --key=edcf99f4e26a4b64b48db6562a7217ac --appID=7a4f26.vidyo.io --userName=.$currernt_user_name. --expiresInSecs=.$expire_time";
	 //$output = shell_exec($command);
	//  $command = exec('C:/xampp/htdocs/drupalnewchat/modules/custom/vidyo_conference/generateToken.py --key=edcf99f4e26a4b64b48db6562a7217ac --appID=7a4f26.vidyo.io --userName=Rutuj --expiresInSecs=10000 2>&1');
	return $token;
 }
  public function getCacheMaxAge() {
    return 0;
  }

}
