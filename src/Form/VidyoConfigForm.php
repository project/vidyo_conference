<?php

namespace Drupal\vidyo_conference\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VidyoConfigForm.
 */
class VidyoConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vidyo_conference.vidyoconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */ 
  public function getFormId() {
    return 'vidyo_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vidyo_conference.vidyoconfig');
    $form['developer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Developer Key'),
      '#description' => $this->t('Please enter developer key which you have created on vidyo.io'),
      '#maxlength' => 32,
      '#size' => 64,
      '#default_value' => $config->get('developer_key'),
    ];
    $form['application_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application ID'),
      '#description' => $this->t('Please enter application id which you created on vidyo.io'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('application_id'),
    ];
    $form['default_expiration_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Expiration Time'),
      '#description' => $this->t('Please enter default expiration time of video chat'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('default_expiration_time'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('vidyo_conference.vidyoconfig')
      ->set('developer_key', $form_state->getValue('developer_key'))
      ->set('application_id', $form_state->getValue('application_id'))
      ->set('default_expiration_time', $form_state->getValue('default_expiration_time'))
      ->save();
  }

}
